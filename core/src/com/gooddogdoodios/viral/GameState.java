package com.gooddogdoodios.viral;

public class GameState {

    public Resource energy;

    /**
     * Construct that can be used to create clones of game states. This
     * is especially handy when you want to mutate the state
     * @param state
     */
    public GameState(GameState state) {
        this.energy = new Resource(state.energy.value, state.energy.increment);
    }

//    private FileSystem enemyFileSystem = new FileSystem();

//    public FileSystem getEnemyFileSystem() {
//        return enemyFileSystem;
//    }
}
