package com.gooddogdoodios.viral;

public class Resource {

    public float value;
    public float increment;

    Resource(float value, float increment) {
        this.value = value;
        this.increment = increment;
    }
}
