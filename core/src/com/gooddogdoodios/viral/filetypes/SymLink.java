package com.gooddogdoodios.viral.filetypes;

import com.gooddogdoodios.viral.ItemSet;
import com.gooddogdoodios.viral.exceptions.DuplicateFoundException;
import com.gooddogdoodios.viral.exceptions.NoDataException;
import com.gooddogdoodios.viral.exceptions.NotDirectoryException;
import com.gooddogdoodios.viral.exceptions.NotSymLinkException;
import com.sun.istack.internal.NotNull;

public class SymLink extends Item {

    private Item reference;

    public SymLink(@NotNull String name, @NotNull Item reference, @NotNull Directory parent) throws DuplicateFoundException{
        this.name = name;
        this.reference = reference;
        this.setParent(parent);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ItemSet getChildren() throws NotDirectoryException {
        return reference.getChildren();
    }

    @Override
    public String getData() throws NoDataException {
        return reference.getData();
    }

    @Override
    public Item getReference() throws NotSymLinkException {
        return reference;
    }

}
