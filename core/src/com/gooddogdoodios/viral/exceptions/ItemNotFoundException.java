package com.gooddogdoodios.viral.exceptions;

public class ItemNotFoundException extends Exception {
    @Override
    public String getMessage() {
        return "File not found.";
    }
}
