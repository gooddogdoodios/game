package com.gooddogdoodios.viral.views;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.gooddogdoodios.viral.GameState;
import com.gooddogdoodios.viral.Viral;

public class MatchMakingScreen implements Screen {

    private Viral orchestrator;

    private Stage stage;

    public MatchMakingScreen(Viral orchestrator) {

        this.orchestrator = orchestrator;

        stage = new Stage(new ScreenViewport());

        Table table = new Table();
        table.setFillParent(true);

        Skin skin = new Skin(Gdx.files.internal("skin/tracer-ui.json"));

        table.add(new Label("Loading...", skin));
        table.row().pad(10, 0, 0, 0);
        table.add(new Label(String.format("Finding match for %s", orchestrator.getLogin()), skin));

        stage.addActor(table);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        checkMatchStatus();
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    private void checkMatchStatus() {
        if (orchestrator.isMatched() == true) {
            orchestrator.changeScreen(orchestrator.PLAYING);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
