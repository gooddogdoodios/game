package com.gooddogdoodios.viral;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.gooddogdoodios.viral.views.LoginScreen;
import com.gooddogdoodios.viral.views.MatchMakingScreen;
import com.gooddogdoodios.viral.views.PlayingScreen;

import java.util.HashMap;

public class Viral extends Game {

    private ServerConnector serverConnector;

    private LoginScreen loginScreen;
    private MatchMakingScreen matchMakingScreen;
    private PlayingScreen playingScreen;

    private GameState myState;
    private GameState oppState;

    private final static float UPDATE_INTERVAL = 5.0f;

    private float timeSinceLastUpdate =  0.0f;

    private boolean matched = false;

    private String login = "Anon";
    private String id = "";
    private String matchId = "";
    private String opponentLogin = "";
    private String opponentId = "";

    public final static int LOGIN = 0;
    public final static int MATCHMAKING = 1;
    public final static int PLAYING = 2;

    @Override
    public void create() {
        loginScreen = new LoginScreen(this);
        serverConnector = new ServerConnector(this);
        this.setScreen(loginScreen);
    }

    @Override
    public void render() {
//        updateState(Gdx.graphics.getDeltaTime());
        super.render();
    }

    /**
     * Broadcast an update to the server after a certain amount of time has passed
     * @param delta
     */
    public void updateState(float delta) {
        if (matched) {
            timeSinceLastUpdate += delta;

            if (timeSinceLastUpdate >= UPDATE_INTERVAL) {
                // @todo: Remove dummy parameter and use the state from the gameState
                serverConnector.broadcastStateUpdate(matchId, this.myState, this.oppState);
                timeSinceLastUpdate = 0;
            }
        }

    }

    /**
     *
     */
    public void connectToServer() {
        serverConnector.connectToServer(login);
    }

    /**
     * @param screen
     */
    public void changeScreen(int screen) {

        switch(screen) {
            case LOGIN:
                if (loginScreen == null) {
                    loginScreen = new LoginScreen(this);
                }
                this.setScreen(loginScreen);
                break;
            case MATCHMAKING:
                if (matchMakingScreen == null) {
                    matchMakingScreen = new MatchMakingScreen(this);
                }
                this.setScreen(matchMakingScreen);
                break;
            case PLAYING:
                if (playingScreen == null) {
                    playingScreen = new PlayingScreen(this);
                }
                this.setScreen(playingScreen);
                break;
            default:
                System.out.println("Error: Unknown screen");
                break;
        }

    }

    /**
     * @return
     *
    public String getLogin() {
        return login;
    }

    /**
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return this.login;
    }

    /**
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return
     */
    public String getOpponentId() {
        return opponentId;
    }

    /**
     * @param opponentId
     */
    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    /**
     * @return
     */
    public String getMatchId() {
        return matchId;
    }

    /**
     * @param matchId
     */
    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public void updateGameState(GameState myState, GameState oppState) {
        serverConnector.broadcastStateUpdate(matchId, myState, oppState);
    }

    public boolean isMatched() {
        return matched;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public GameState getMyState() {
        return myState;
    }

    public void setMyState(GameState myState) {
        this.myState = myState;
    }

    public GameState getOppState() {
        return oppState;
    }

    public void setOppState(GameState oppState) {
        this.oppState = oppState;
    }

    public String getOpponentLogin() {
        return opponentLogin;
    }

    public void setOpponentLogin(String opponentLogin) {
        this.opponentLogin = opponentLogin;
    }
}

