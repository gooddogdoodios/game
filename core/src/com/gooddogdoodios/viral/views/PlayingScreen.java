package com.gooddogdoodios.viral.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.gooddogdoodios.viral.CommandParser;
import com.gooddogdoodios.viral.GameState;
import com.gooddogdoodios.viral.Viral;

import java.util.ArrayList;

public class PlayingScreen implements Screen {

    private final static int screenWidth = 1024;
    private final static int screenHeight = 768;

    private Viral orchestrator;
    private CommandParser commandParser;
    private Skin skin;
    private Stage stage;
    private Music bgLoop;
    private String command;
    private ArrayList<String> history;
    private GameState myState;
    private GameState oppState;

    private Label energy;
    private Label energyInc;

    public PlayingScreen(Viral orchestrator) {

        this.orchestrator = orchestrator;
        this.myState = orchestrator.getMyState();
        this.oppState = orchestrator.getOppState();

        commandParser = new CommandParser(orchestrator, myState);

        history = new ArrayList<>();

        stage = new Stage(new ScreenViewport());


        bgLoop = Gdx.audio.newMusic(Gdx.files.internal("game.wav"));
        bgLoop.setLooping(true);
        bgLoop.play();

        skin = new Skin(Gdx.files.internal("skin/tracer-ui.json"));

        final TextField commandField = new TextField(command, skin);
        commandField.setWidth(screenWidth);

        Label enemy = new Label(String.format("Playing Against: %s", orchestrator.getOpponentLogin()), skin);
        this.energy = new Label("Energy: ", skin);
        this.energyInc = new Label("Energy Increment: ", skin);

        enemy.setY(758 - enemy.getHeight());
        this.energy.setY(758 - enemy.getHeight() - this.energy.getHeight());
        this.energyInc.setY(758 - enemy.getHeight() - this.energy.getHeight() - this.energyInc.getHeight());

        stage.addActor(enemy);
        stage.addActor(this.energy);
        stage.addActor(this.energyInc);

        stage.setKeyboardFocus(commandField);

        stage.addListener(new InputListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ENTER) {
                    String enteredCommand = String.format("%s : %s", orchestrator.getLogin(), commandField.getText());
                    Label enteredLabel = new Label(enteredCommand, skin);
                    history.add(enteredCommand);
                    enteredLabel.setY(700 - history.size() * enteredLabel.getHeight());
                    String parsedCommand = commandParser.parseCommand(commandField.getText());
                    history.add(parsedCommand);
                    Label nextCommand = new Label(parsedCommand, skin);
                    nextCommand.setY(700 - history.size() * nextCommand.getHeight());
                    nextCommand.setColor(1.0f, 1.0f, .5f, 1.0f);
                    stage.addActor(enteredLabel);
                    stage.addActor(nextCommand);
                    commandField.setText("");
                    return true;
                } else if (keycode == Input.Keys.UP) {
                    if (history.size() > 0) {
                        String lastCommand = history.get(history.size() - 1);
                        commandField.setText(lastCommand);
                    }
                }
                return false;
            }
        });

        stage.addActor(commandField);

    }

    @Override
    public void show() { Gdx.input.setInputProcessor(stage); };

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        myState = orchestrator.getMyState();
        this.energy.setText(String.format("Energy: %s", myState.energy.value));
        this.energyInc.setText(String.format("Energy Increment: %s", myState.energy.increment));
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
    }
}
