package com.gooddogdoodios.viral;

import com.badlogic.gdx.Gdx;
import com.google.gson.Gson;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerConnector {

//    private static final String hostName = "http://secure-coast-24974.herokuapp.com";
    private static final String hostName = "http://localhost:8080";

    private static final String LOGIN_EVENT = "login";
    private static final String LOGIN_COMPLETE_EVENT = "socketID";
    private static final String MATCH_FOUND_EVENT = "matchFound";
    private static final String STATE_UPDATE_EVENT = "stateUpdate";

    private Gson gson;
    private Socket socket;
    private Viral orchestrator;

    /**
     * @param orchestrator
     */
    public ServerConnector(Viral orchestrator) {
        gson = new Gson();
        this.orchestrator = orchestrator;
    }

    /**
     * @param login
     */
    public void connectToServer(String login) {
        try {
            socket = IO.socket(hostName);
            socket.connect();
            doLogin(login);
            listenForEvents();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * @param login
     */
    private void doLogin(String login) {
        try {
            JSONObject args = new JSONObject();
            args.put("login", login);
            socket.emit(LOGIN_EVENT, args);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     *
     */
    private void listenForEvents() {
        socket.on(Socket.EVENT_CONNECT, args -> {
        }).on(LOGIN_COMPLETE_EVENT, args -> {
            JSONObject data = (JSONObject) args[0];
            try {
                this.orchestrator.setId(data.getString("id"));
            } catch (JSONException e) {
                Gdx.app.log("SocketIO", "Error Getting ID");
            }
        }).on(MATCH_FOUND_EVENT, this::handleMatchFound)
        .on(STATE_UPDATE_EVENT, this::handleStateUpdate);

    }

    /**
     * @param args
     */
    private void handleStateUpdate(Object... args) {
        try {
            JSONObject update = (JSONObject) args[0];
            GameState myState = gson.fromJson(update.getJSONObject("state").getJSONObject(orchestrator.getId()).toString(), GameState.class);
            GameState oppState = gson.fromJson(update.getJSONObject("state").getJSONObject(orchestrator.getOpponentId()).toString(), GameState.class);
            orchestrator.setMyState(myState);
            orchestrator.setOppState(oppState);
        }  catch (JSONException e) {
            Gdx.app.log("SocketIO", "Could not update state");
        }
    }

    /**
     * @param args
     */
    private void handleMatchFound(Object... args) {
        try {
            JSONObject update = (JSONObject) args[0];
            this.orchestrator.setOpponentId(update.getString("opponentId"));
            this.orchestrator.setOpponentLogin(update.getString("opponent"));
            this.orchestrator.setMatchId(update.getString("matchId"));
            handleStateUpdate(update);
            orchestrator.setMatched(true);
        } catch(JSONException e) {
            Gdx.app.log("SocketIO", "Error: Could not find a match");
        }

    }

    /**
     * Broadcast update to the server so other players are given an updated
     * copy of the state
     * @param matchId
     * @param myState
     * @param oppState
     */
    public void broadcastStateUpdate(String matchId, GameState myState, GameState oppState) {
        JSONObject update = new JSONObject();
        try {
            update.put("matchId", matchId);
            update.put(orchestrator.getId(), gson.toJson(myState));
            update.put(orchestrator.getOpponentId(), gson.toJson(oppState));
            socket.emit("changeState", update);
        } catch (Exception e) {
            // handle
        }
    }

}
