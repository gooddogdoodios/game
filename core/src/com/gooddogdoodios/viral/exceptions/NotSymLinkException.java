package com.gooddogdoodios.viral.exceptions;

public class NotSymLinkException extends Exception {
    @Override
    public String getMessage() {
        return "This file is not a symlink.";
    }
}
