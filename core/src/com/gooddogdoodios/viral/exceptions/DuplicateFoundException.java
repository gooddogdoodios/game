package com.gooddogdoodios.viral.exceptions;

public class DuplicateFoundException extends Exception {
    @Override
    public String getMessage() {
        return "Can't add to directory, there was another file of the same name";
    }
}
