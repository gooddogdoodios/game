package com.gooddogdoodios.viral.filetypes;

import com.gooddogdoodios.viral.ItemSet;
import com.gooddogdoodios.viral.exceptions.DuplicateFoundException;
import com.gooddogdoodios.viral.exceptions.NoDataException;
import com.gooddogdoodios.viral.exceptions.NotDirectoryException;
import com.gooddogdoodios.viral.exceptions.NotSymLinkException;
import com.sun.istack.internal.NotNull;

public class Directory extends Item {

    private ItemSet children;

    /**
     * Primary constructor for directories.
     * @param name Name of the directory
     * @param parent
     * @param children
     */
    public Directory(@NotNull String name, @NotNull Directory parent, ItemSet children) throws DuplicateFoundException {
        this.name = name;
        this.setParent(parent);
        this.children = children == null ? new ItemSet() : children;
        if (!this.children.containsItem("..")) {
            new SymLink("..", this.parent, this);
        }
    }

    private Directory(@NotNull String name, ItemSet children) {
        this.name = name;
        this.children = children == null ? new ItemSet() : children;
    }

    public static Directory getRootDirectory(ItemSet items) {
        return new Directory("/", items);
    }

    public boolean isRoot() {
        return this.parent == null;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Directory getParent() {
        return this.parent;
    }

    @Override
    public ItemSet getChildren() throws NotDirectoryException {
        return children;
    }

    @Override
    public String getData() throws NoDataException {
        throw new NoDataException();
    }

    @Override
    public Item getReference() throws NotSymLinkException {
        throw new NotSymLinkException();
    }

    public void add(Item child) throws DuplicateFoundException {
        this.children.addItem(child);
    }
}

