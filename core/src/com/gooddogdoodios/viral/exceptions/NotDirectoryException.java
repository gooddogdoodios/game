package com.gooddogdoodios.viral.exceptions;

public class NotDirectoryException extends Exception {
    @Override
    public String getMessage() {
        return "This file is not a directory.";
    }
}
