package com.gooddogdoodios.viral.filetypes;

import com.gooddogdoodios.viral.ItemSet;
import com.gooddogdoodios.viral.exceptions.DuplicateFoundException;
import com.gooddogdoodios.viral.exceptions.NoDataException;
import com.gooddogdoodios.viral.exceptions.NotDirectoryException;
import com.gooddogdoodios.viral.exceptions.NotSymLinkException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Item {

    Directory parent;
    String name;

    /**
     * Get the name of the item in the file system.
     * @return guddogdoodios and their puppers
     */
    public abstract String getName();

    /**
     * Gets the name of the file in the context of the directories it is in
     * @return fully qualified name of the file in the directory structure
     */
    public String getQualifiedName() {
        if (this.parent == null) {
            return this.name;
        } else {
            List<String> result = new ArrayList<>();
            Item currentItem = this;
            while (currentItem != null) {
                result.add(currentItem.getName());
                currentItem = currentItem.getParent();
            }
            Collections.reverse(result);
            StringBuilder qualifiedName = new StringBuilder();
            for (String itemName : result) {
                if (qualifiedName.length() == 0) {
                    qualifiedName.append(itemName);
                } else {
                    qualifiedName.append(itemName);
                    qualifiedName.append(this instanceof Directory ? "/" : "");
                }
            }
            return qualifiedName.toString();
        }
    }

    /**
     * @return {@link Directory} that is the parent of this item.
     */
    public Directory getParent() {
        return this.parent;
    }


    /**
     * Sets the parent for an item.
     */
    void setParent(Directory parent) throws DuplicateFoundException {
        parent.add(this);
        this.parent = parent;
    }

    /**
     * Get the children of the item if they exist.
     * @return
     * @throws NotDirectoryException
     */
    public abstract ItemSet getChildren() throws NotDirectoryException;

    /**
     * Get the data associated with an item, and throw an exception
     * if there is no data associated with this item.
     * @return guddogdoodios and their puppers if all is kosher, sadness otherwise
     */
    public abstract String getData() throws NoDataException;

    public abstract Item getReference() throws NotSymLinkException;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Item && this.getQualifiedName().equals(((Item) obj).getQualifiedName());
    }
}
