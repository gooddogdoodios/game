package com.gooddogdoodios.viral.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gooddogdoodios.viral.Viral;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Viral";
		config.width = 1024;
		config.height = 768;
		new LwjglApplication(new Viral(), config);
	}
}
