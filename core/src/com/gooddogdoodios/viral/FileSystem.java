package com.gooddogdoodios.viral;

import com.gooddogdoodios.viral.exceptions.DuplicateFoundException;
import com.gooddogdoodios.viral.exceptions.ItemNotFoundException;
import com.gooddogdoodios.viral.exceptions.NotDirectoryException;
import com.gooddogdoodios.viral.exceptions.NotSymLinkException;
import com.gooddogdoodios.viral.filetypes.Directory;
import com.gooddogdoodios.viral.filetypes.Item;
import com.gooddogdoodios.viral.filetypes.SymLink;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileSystem {

    private Directory presentWorkingDirectory;
    private Directory root;

    public FileSystem() {
        // setting starting directory to root
        this.root = Directory.getRootDirectory(null);
        this.presentWorkingDirectory = this.root;

        List<String> topLevelDirs = Arrays.asList("var", "www", "etc", "dev", "logs", "lib");
        List<Item> lst = new ArrayList<>();
        List<Item> sublst = new ArrayList<>();
        for (String name : topLevelDirs) {
            try {
                Directory dir = new Directory(name, root, null);
                lst.add(dir);
                sublst.add(new Directory(name + "_stuff", dir, null));
            } catch (DuplicateFoundException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Show the contents of the present working directory
     * @return List of the names of the files in the current directory
     */
    public List<String> ls() throws NotDirectoryException {
        final ItemSet items = presentWorkingDirectory.getChildren();
        return items.stream().map(Item::getName).collect(Collectors.toList());
    }

    /**
     * @return the qualified location of the {@link FileSystem#presentWorkingDirectory}
     */
    public String pwd() {
        return this.presentWorkingDirectory.getQualifiedName();
    }

    /**
     * Changes the {@link FileSystem#presentWorkingDirectory} to whatever directory is specified
     * @param itemName the specified directory name to search for
     * @throws ItemNotFoundException this case will occur if you try to cd into
     * something that doesn't exist
     * @throws NotDirectoryException this case will occur if you try to cd into
     * something that is not a directory
     */
    public void cd(String itemName) throws ItemNotFoundException, NotDirectoryException, NotSymLinkException {
        final Item item = presentWorkingDirectory
                .getChildren()
                .getItem(itemName)
                .orElseThrow(ItemNotFoundException::new);

        this.setWorkingDirectory(item);
    }

    /**
     * @param item
     * @throws NotSymLinkException
     * @throws NotDirectoryException
     */
    private void setWorkingDirectory(Item item) throws NotSymLinkException, NotDirectoryException {
        if (item instanceof Directory) {
            this.presentWorkingDirectory = (Directory) item;
        } else if (item instanceof SymLink) {
            setWorkingDirectory(item.getReference());
        } else {
            throw new NotDirectoryException();
        }
    }

}

