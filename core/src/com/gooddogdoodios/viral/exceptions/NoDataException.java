package com.gooddogdoodios.viral.exceptions;

public class NoDataException extends Exception {
    @Override
    public String getMessage() {
        return "No data found for this file.";
    }
}
