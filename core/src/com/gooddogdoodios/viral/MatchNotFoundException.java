package com.gooddogdoodios.viral;

import org.json.JSONException;

public class MatchNotFoundException extends JSONException {

    public MatchNotFoundException(String s) {
        super(s);
    }

    public MatchNotFoundException(Throwable throwable) {
        super(throwable);
    }
}
