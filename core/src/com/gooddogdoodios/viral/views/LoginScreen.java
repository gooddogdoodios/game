package com.gooddogdoodios.viral.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.gooddogdoodios.viral.GameState;
import com.gooddogdoodios.viral.Viral;


public class LoginScreen implements Screen {

    private Viral orchestrator;
    private Stage stage;
    private Music bgLoop;

    /**
     * @param orchestrator
     */
    public LoginScreen(final Viral orchestrator) {

        this.orchestrator = orchestrator;

        stage = new Stage(new ScreenViewport());

        Table table = new Table();
        table.setFillParent(true);

        Skin skin = new Skin(Gdx.files.internal("skin/tracer-ui.json"));

        stage.addActor(table);
        Label loginLabel = new Label("Login", skin);
        final TextField loginField = new TextField("Anon", skin);
        table.add(loginLabel).fillX().uniformX();
        table.add(loginField).fillX().uniformX().colspan(3);
        table.row().pad(10, 0, 10, 0);
        TextButton newGame = new TextButton("New Game", skin);
        TextButton exit = new TextButton("Exit", skin);

        newGame.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                orchestrator.setLogin(loginField.getText());
                orchestrator.connectToServer();
                bgLoop.stop();
                orchestrator.changeScreen(orchestrator.MATCHMAKING);
            }
        });

        exit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        table.add(newGame).fillX().uniformX();
        table.add(exit).fillX().uniformX();

        bgLoop =  Gdx.audio.newMusic(Gdx.files.internal("loop.wav"));
        bgLoop.setLooping(true);
        bgLoop.play();

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}
