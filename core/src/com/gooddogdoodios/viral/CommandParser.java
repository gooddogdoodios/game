package com.gooddogdoodios.viral;

import java.util.Arrays;
import java.util.List;

public class CommandParser {

    private Viral orchestrator;
    private GameState gameState;
    private String PROMPT_CHAR = ">";

    /**
     *
     * @param gameState
     * @param orchestrator
     */
    public CommandParser(Viral orchestrator, GameState gameState) {
        this.orchestrator = orchestrator;
        this.gameState = gameState;
    }

    public String parseCommand(String result) {
        List<String> arguments = Arrays.asList(result.split("\\s"));
        if (arguments.size() > 0) {
            final String command = arguments.get(0);
            switch (command) {
                case "ime":
                    return ime();
                case "help":
                    return help();
                case "ls":
                    return ls();
                case "cd":
                    return cd(arguments);
                default:
                    return prompt("Error: Invalid Command");
            }
        } else {
            return prompt("Error: Invalid Command");
        }
    }

    private String ime() {
        GameState oppState = orchestrator.getOppState();
        GameState myState = new GameState(orchestrator.getMyState());
        myState.energy.increment = 100.0f;
        orchestrator.updateGameState(myState, oppState);
        return prompt("MUTATION");
    }

    private String cd(List<String> args) {
        if (args.size() <= 1) {
            return prompt("Usage: cd <desired_directory>");
        } else {
            final List<String> rest = args.subList(1, args.size());
            if (rest.size() > 1) {
                return prompt("Usage: cd <desired_directory>");
            } else {
//            try {
//               this.gameState.getEnemyFileSystem().cd(rest.get(0));
                return "";
//            } catch (FileNotFoundException e) {
//               return prompt("Error: File not found");
//            }
            }
        }
    }

    /**
     * Send back the contents of the current working directory
     * @return
     */
    private String ls() {
//      final List<String> results = this.gameState.getEnemyFileSystem().ls();
//      return prompt(results.stream().reduce("", (l, r) -> l + " " + r));
        return "FUCK";
    }

    private String help() {
        return prompt("Learn to help yourself...");
    }

    private String prompt(String input) {
        return PROMPT_CHAR + " " + input;
    }

}
