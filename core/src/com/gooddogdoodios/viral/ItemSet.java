package com.gooddogdoodios.viral;

import com.gooddogdoodios.viral.exceptions.DuplicateFoundException;
import com.gooddogdoodios.viral.filetypes.Item;

import java.util.HashSet;
import java.util.Optional;

public class ItemSet extends HashSet<Item> {

    public void addItem(Item item) throws DuplicateFoundException {
        if (this.stream().map(Item::getName).anyMatch(item.getName()::equals) || !super.add(item)) {
            throw new DuplicateFoundException();
        }
    }

    public boolean containsItem(String dirName) {
        return this.stream().map(Item::getName).anyMatch(dirName::equals);
    }

    public Optional<Item> getItem(String name) {
        return this.stream().filter(i -> i.getName().equals(name)).findFirst();
    }

}
